### React.JS Chart ###

Quick summaryThis is the source for the 1st Quiz Challenge, which was meant to make a simple chart.

### How do I get set up? ###

To test it you just need to run `npm start` (having the dependencies: node, react.js...)
It works with the `data` prop:
```
<Chart
	data={
	[
		{
			prompt: "string",
			data: [0, 1, 2, 3, 4]
		}
	]
/>
```

### Result Image ###

https://i.imgur.com/ZKhoTN5.jpg

### ---
* Made by Lucas Coelho da Costa - lcrabbit