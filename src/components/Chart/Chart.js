import React from 'react';
import './Chart.css'


const VerticalSeparator = ({ left }) => {
    return (

        <div
            className="vertical-separator"
            style={{ left: `${left}px` }}
        />
    );
}
const ChartBar = ({ percent, color, marginLeft, borderRight }) => {
    return (

        <div className="chart-bar" style={{
            width: `${percent}%`, background: `${color}`,
            marginLeft: `${marginLeft}%`,
            borderRightWidth: `${borderRight}px`,
            borderRightColor: '#FFF',
            borderRightStyle: 'solid',
        }} />
    )
}

class Chart extends React.Component {

    state = {
        barColors: ['#c51b7d', '#de73aa', '#d1d1d1', '#83b954', '#4d9221'],
        separatorsToRow: 5,
        initialPositionModifier: 207,
        offsetModifier: 167,
    }

    renderPercentages(data) {
        return data.map((item, index) => {
            return <p className="percentage" style={{ marginLeft: `${index === 0 ? 310 : 120}px` }}>{item}%</p>
        });
    }

    renderSeparators() {
        return Array(this.state.separatorsToRow).fill(4).map((_, i) => (
            <VerticalSeparator
                left={i === 0 ? this.state.initialPositionModifier : this.state.initialPositionModifier + (i * this.state.offsetModifier)}
                key={i}
            />
        ));
    }

    getPercentage = (percentages, i) => {
        const sum = percentages.reduce((currently_sum, num) => currently_sum + num);
        let percent = Math.floor(percentages[i] / sum * 100);
        return percent / 2;

    }

    getLeftMargin = (percentages, i) => {
        let stronglyAgree = this.getPercentage(percentages, 4);
        let agree = this.getPercentage(percentages, 3);
        let stronglyDisagree = this.getPercentage(percentages, 0);
        let disagree = this.getPercentage(percentages, 1);
        return (stronglyAgree + agree) - (stronglyDisagree + disagree);
    }

    renderChartBars(prompt, data) {
        return (
            <div className="flex-data">
                <label className="prompt">{prompt}</label>
                <div className="bar-wrapper">
                    {data.map((item, i) => (
                        <ChartBar
                            percent={this.getPercentage(data, i)}
                            marginLeft={i === 0 && this.getLeftMargin(data, i)}
                            borderRight={item !== 0 && i < data[i + 1] > 0 ? 1 : 0}
                            color={this.state.barColors[i]}
                            key={i}
                        />
                    ))}
                </div>
            </div>
        )
    }

    render() {

        const { data } = this.props;
        return (
            <div className="chart-wrapper">
                <div className="chart">
                    <div className="line-separators-container">
                        {data.map((item, _) => {
                            return this.renderChartBars(item.prompt, item.data);
                        })}
                    </div>
                    <div className="separator">{this.renderSeparators()}</div>
                </div>
                <div className="percentages-container">{this.renderPercentages([100, 50, 0, 50, 100])}</div>
            </div>
        );
    }
}

export default Chart;