import React, { Component } from 'react';
import Chart from './components/Chart/Chart';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Chart
          data={[
            {
              prompt: "My issue was resolved in a timely manner",
              data: [1, 14, 24, 3, 4]
            }, {
              prompt: "The representative understood me",
              data: [0, 3, 23, 15, 10]
            },
            {
              prompt: "I would recommend the service to others",
              data: [8, 7, 8, 9, 5]
            }]
          }
        />
      </div>
    );
  }
}

export default App;
